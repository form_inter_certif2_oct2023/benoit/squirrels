#' data_act_squirrels
#'
#' A sample of the dataset nyc_squirrels_act_sample.csv
#'
#' @format A data frame with 15 rows and 4 variables:
#' \describe{
#'   \item{ age }{  character. The age of the squirrels }
#'   \item{ primary_fur_color }{  character }
#'   \item{ activity }{  character }
#'   \item{ counts }{  numeric }
#' }
#' @source Source
"data_act_squirrels"
