
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels

<!-- badges: start -->
<!-- badges: end -->

L’objectif de ce package est d’etudier les fichiers sur des ecureuils

Utilisation d’une branche pour modifier mon readme

## Installation

You can install the development version of squirrels like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrels)
## basic example code
```

``` r
get_message_fur_color(primary_fur_color = "Black")
#> We will focus on Black squirrels

check_primary_color_is_ok(string = "Gray")
#> [1] TRUE
```

## Code of Conduct

Please note that the squirrels project is released with a [Contributor
Code of
Conduct](https://contributor-covenant.org/version/2/1/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.
